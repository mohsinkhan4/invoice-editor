import _ from 'lodash';

import { invoiceItems } from './index';
import { 
    CHANGE,
    ADD,
    REMOVE
} from '../actions';

const initialState = {
    tableData: [
        { key: '1', item : 'Widget', qty: 2, price: 10, total: 20 },
        { key: '2', item : 'Cog', qty: 2, price: 15.99, total: 31.98 }
    ],
    newData: {
        key: 'new', item : 'Widget', qty: 2, price: 10, total: 20
    },
    subtotal: 51.98,
    taxPercentage: 10,
    tax: 5.2,
    total: 57.18
}

describe('invoiceItems', () => {
    it('should calculate the item total when qty and price are entered', () => {
        const { tableData } = invoiceItems(
            initialState,
            {
                type: CHANGE,
                key: '1',
                field: 'price',
                value: 20
            }
        );
        expect(_.find(tableData, { key: '1' }).total).toEqual(40);
    });

    it('should add a new item when add action is dispatched', () => {
        const { tableData } = invoiceItems(
            initialState,
            {
                type: ADD,
                key: 'somekey'
            }
        );
        expect(tableData).toHaveLength(3);
    });

    it('should remove the specified item when remove action is dispatched', () => {
        const { tableData } = invoiceItems(
            initialState,
            {
                type: REMOVE,
                key: '1'
            }
        );
        expect(tableData).toHaveLength(1);
    });
});
