import { combineReducers } from 'redux';

import helpers from './helpers';
import { 
    CHANGE,
    CHANGE_NEW,
    CHANGE_TAX_PERCENTAGE,
    ADD,
    REMOVE
} from '../actions'; 

/**
 * constants
 * 
 */

const { calculateDetailsData, calculateNewData, calculateSummation } = helpers;
const newDataInit = {
    key: 'NEW',
    item : '',
    qty: '',
    price: ''
};

/**
 * reducers
 */

export const invoiceItems = (
    state = {
        tableData: [],
        newData: newDataInit,
        subtotal: 0,
        taxPercentage: 5,
        tax: 0,
        total: 0
    },
    action ) => {
    
    const { tableData, newData, subtotal, taxPercentage } = state;
    const { key = '', field = '', value = '' } = action;

    switch (action.type) {

        case CHANGE:
            return {
                ...state,
                ...calculateSummation({ 
                    tableData : calculateDetailsData({ tableData, key, field, value }), 
                    taxPercentage 
                })
            };
        
        case CHANGE_NEW: 
            return {
                ...state,
                newData: calculateNewData({ newData, field, value })
            };
        
        case CHANGE_TAX_PERCENTAGE:
            return {
                ...state,
                taxPercentage: value,
                ...calculateSummation({ tableData, subtotal, taxPercentage: value })
            };
            
        case ADD:
            return {
                ...state,
                newData: newDataInit,
                ...calculateSummation({ 
                    tableData: tableData.concat({
                        ...newData,
                        key: key
                    }),
                    taxPercentage
                })
            };

        case REMOVE:
            return {
                ...state,
                ...calculateSummation({ 
                    tableData: tableData.filter((rowData) => (rowData.key !== key)),
                    taxPercentage
                })
            };

        default:
            return state;

    }
}

const invoiceApp = combineReducers({
    invoiceItems
})

export default invoiceApp;