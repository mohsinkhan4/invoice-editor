import _ from 'lodash';

const calculateItemTotal = ({ qty, price }) => (
    (_.isFinite(qty) && _.isFinite(price)) ? (qty * price) : 0);

const calculateDetailsData = ({ tableData, key, field, value }) => (
    tableData.map((rowData) => {
        if(rowData.key !== key) { return rowData; }
        const n = {
            ...rowData,
            [field]: value
        };
        if(!_.includes(['qty', 'price'], field)) { return n; }
        return {
            ...n,
            total : calculateItemTotal(n)
        }
    })
)

const calculateNewData = ({ newData, field, value }) => {
    const n = {
        ...newData,
        [field]: value
    };
    if(!_.includes(['qty', 'price'], field)) { return n; }
    return {
        ...n,
        total : calculateItemTotal(n)
    }
}

const calculateSubtotal = ({ tableData, subtotal }) => {
    return (tableData ? _.round(tableData.reduce((acc, d) => (acc + d.total), 0), 2) : subtotal);
}

const calculateTax = ({ tableData, subtotal, taxPercentage }) => {
    const mSubtotal = calculateSubtotal({tableData, subtotal});
    return {
        mSubtotal,
        tax: (_.isFinite(taxPercentage) ? _.round(mSubtotal * (taxPercentage / 100), 2) : 0)
    };
}

const calculateSummation = ({ tableData, subtotal, taxPercentage }) => {
    const {mSubtotal, tax} = calculateTax({tableData, subtotal, taxPercentage});
    return {
        tableData,
        subtotal: mSubtotal,
        tax,
        total: (mSubtotal + tax)
    };
}

export default {
    calculateItemTotal,
    calculateDetailsData,
    calculateNewData,
    calculateSubtotal,
    calculateTax,
    calculateSummation
}