import _ from 'lodash';

import helpers from './helpers';

const { 
    calculateItemTotal,
    calculateDetailsData,
    calculateNewData,
    calculateSubtotal,
    calculateTax,
    calculateSummation
} = helpers;

const mockTableData = [
    { key: '1', item : 'Widget', qty: 2, price: 10, total: 20 },
    { key: '2', item : 'Cog', qty: 2, price: 15.99, total: 31.98 }
];

const mockNewData = {
    key: 'new', item : 'Widget', qty: 2, price: 10, total: 20
};

describe('calculateItemTotal', () => {
    it('should calculate the item total when qty and price are entered', () => {
        const total = calculateItemTotal({ qty: 1, price: 1 });
        expect(total).toEqual(1);
    });

    it('should default to 0 when qty and price are  not entered', () => {
        const total = calculateItemTotal({ });
        expect(total).toEqual(0);
    });
});

describe('calculateDetailsData', () => {
    it('should calculate the modified item total for an item when price is changed', () => {
        const tableData = calculateDetailsData({
            tableData: mockTableData,
            key: '1',
            field: 'price',
            value: 20
        });
        expect(_.find(tableData, { key: '1' }).total).toEqual(40);
    });

    it('should calculate the modified item total for an item when qty is changed', () => {
        const tableData = calculateDetailsData({
            tableData: mockTableData,
            key: '2',
            field: 'qty',
            value: 1
        });
        expect(_.find(tableData, { key: '2' }).total).toEqual(15.99);
    });
});

describe('calculateNewData', () => {
    it('should calculate the modified item total for an item when price is changed', () => {
        const newData = calculateNewData({
            newData: mockNewData,
            field: 'price',
            value: 20
        });
        expect(newData.total).toEqual(40);
    });

    it('should calculate the modified item total for an item when qty is changed', () => {
        const newData = calculateNewData({
            newData: mockNewData,
            field: 'qty',
            value: 1
        });
        expect(newData.total).toEqual(10);
    });
});

describe('calculateSubtotal', () => {
    it('should calculate the modified subtotal if tableData is supplied', () => {
        const subtotal = calculateSubtotal({ tableData: mockTableData, subtotal: 100 });
        expect(subtotal).toEqual(51.98);
    });

    it('should return original subtotal if tableData is not supplied', () => {
        const subtotal = calculateSubtotal({ subtotal: 100 });
        expect(subtotal).toEqual(100);
    });
});

describe('calculateTax', () => {
    it('should calculate the tax based on tableData and taxPercentage supplied', () => {
        const { tax } = calculateTax({ tableData: mockTableData, taxPercentage: 10 });
        expect(tax).toEqual(5.2);
    });
});

describe('calculateSummation', () => {
    it('should calculate the total based on tableData and taxPercentage supplied', () => {
        const { total } = calculateSummation({ tableData: mockTableData, taxPercentage: 10  });
        expect(total).toEqual(57.18);
    });
});