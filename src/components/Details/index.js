import React from 'react';

import DetailsHeader from './Header';
import DetailsBody from './Body';

const Details = (props) => (
    <table className="datatable">
        <DetailsHeader />
        <DetailsBody 
            {...props}
        />
    </table>
);

export default Details;