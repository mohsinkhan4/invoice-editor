import React from 'react';

const DetailsHeader = () => {
    return (
        <thead>
            <tr>
                <th>Item</th>
                <th>Qty</th>
                <th>Price</th>
                <th>Total</th>
                <th></th>
            </tr>
            <tr>
                <th>----------------------</th>
                <th>--------------</th>
                <th>--------------</th>
                <th>--------------</th>
                <th>-------</th>
            </tr>
        </thead>
    )
}

export default DetailsHeader;