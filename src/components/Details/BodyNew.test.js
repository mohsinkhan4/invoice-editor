import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

import DetailsBodyNew from './BodyNew';

Enzyme.configure({ adapter: new Adapter() });

const mockNewData = {
    key: 'new', item : 'Widget', qty: 2, price: 10, total: 20
};
const onChangeNew = jest.fn(() => {});
const onAdd = jest.fn(() => {});

describe('DetailsBodyNew', () => {
    it('should contain a row and 5 columns', () => {
        const wrapper = shallow(<DetailsBodyNew />);

        expect(wrapper.find('tr')).toHaveLength(1);
        expect(wrapper.find('tr td')).toHaveLength(5);
    });

    it('should render the correct values', () => {
        const wrapper = shallow(<DetailsBodyNew newData={mockNewData} />);

        expect(wrapper.find('.itemNew').get(0).props.value).toEqual('Widget');
        expect(wrapper.find('.qtyNew').get(0).props.value).toEqual(2);
        expect(wrapper.find('.priceNew').get(0).props.value).toEqual(10);
        expect(wrapper.find('.totalNew').text()).toContain('20');
    });

    it('should call the onAdd callback on when data is not entered', () => {
        const wrapper = shallow(<DetailsBodyNew onAdd={onAdd}/>);
        wrapper.find('.onAdd').simulate('click');

        expect(onAdd).not.toHaveBeenCalled();
    });

    it('should call the callbacks on necessary actions', () => {
        const wrapper = shallow(<DetailsBodyNew newData={mockNewData} onChangeNew={onChangeNew} onAdd={onAdd}/>);
        wrapper.find('.itemNew').simulate('change', { target: { value: 'NewItem' } });
        wrapper.find('.onAdd').simulate('click');

        expect(onChangeNew).toHaveBeenCalledWith({
            field: 'item', 
            value: 'NewItem'
        });
        expect(onAdd).toHaveBeenCalled();
    });
});