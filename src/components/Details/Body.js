import React from 'react';

import DetailsBodyNew from './BodyNew';

const DetailsBody = (props) => {
    const { tableData = [], onChange, onRemove } = props;
    return (
        <tbody>
            {
                tableData.map(({ key, item, qty, price, total }) => (
                    <tr key={key}>
                        <td>
                            <input 
                                className="item"
                                onChange={(e) => onChange({
                                    key,
                                    field: 'item',
                                    value: e.target.value
                                })}
                                value={item}
                            />
                        </td>
                        <td>
                            [ <input
                                type="number"
                                className="qty"
                                onChange={(e) => onChange({
                                    key,
                                    field: 'qty',
                                    value: parseInt(e.target.value, 10)
                                })}
                                value={qty}
                            /> ]
                        </td>
                        <td>
                            [ <input
                                type="number"
                                className="price"
                                onChange={(e) => onChange({
                                    key,
                                    field: 'price',
                                    value: parseFloat(e.target.value, 10)
                                })}
                                value={price}
                            /> ]
                        </td>
                        <td className="total">
                            [ {total} ]
                        </td>
                        <td>
                            <button 
                                className="onRemove"
                                onClick={() => (onRemove(key))}
                            >
                                x
                            </button>
                        </td>
                    </tr>
                ))
            }
            <DetailsBodyNew 
                {...props}
            />
        </tbody>
    );
}

export default DetailsBody;