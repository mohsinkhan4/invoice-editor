import React from 'react';
import _ from 'lodash';

const DetailsBodyNew = ({ newData = {}, onChangeNew, onAdd }) => {
    return (    
        <tr key={newData.key}>
            <td>
                [ <input 
                    className="item itemNew"
                    placeholder="New Item"
                    onChange={(e) => onChangeNew({
                        field: 'item',
                        value: e.target.value
                    })}
                    value={newData.item}
                /> ]
            </td>
            <td>
                [ <input
                    type="number"
                    className="qty qtyNew"
                    onChange={(e) => onChangeNew({
                        field: 'qty', 
                        value: parseInt(e.target.value, 10)
                    })}
                    value={newData.qty}
                /> ]
            </td>
            <td>
                [ <input
                    type="number"
                    className="price priceNew"
                    onChange={(e) => onChangeNew({
                        field: 'price',
                        value: parseFloat(e.target.value, 10)
                    })}
                    value={newData.price}
                /> ]
            </td>
            <td
                className="total totalNew"
            >
                [ {newData.total && _.round(newData.total, 2)} ]
            </td>
            <td>
                <button
                    className="onAdd"
                    onClick={newData.total && onAdd}
                >
                    +
                </button>
            </td>
        </tr>
    );
}

export default DetailsBodyNew;