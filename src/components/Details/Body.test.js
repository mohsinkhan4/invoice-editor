import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

import DetailsBody from './Body';

Enzyme.configure({ adapter: new Adapter() });

const mockTableData = [
    { key: '1', item : 'Widget', qty: 2, price: 10, total: 20 },
    { key: '2', item : 'Cog', qty: 2, price: 15.99, total: 31.98 }
];
const onChange = jest.fn(() => {});
const onRemove = jest.fn(() => {});

describe('DetailsBody', () => {
    it('should contain rows based on tableData entered and 5 columns per row', () => {
        const wrapper = shallow(<DetailsBody tableData={mockTableData} />);

        expect(wrapper.find('tr')).toHaveLength(2);
        expect(wrapper.find('tr td')).toHaveLength(10);
    });

    it('should render the correct values', () => {
        const wrapper = shallow(<DetailsBody tableData={mockTableData} />);

        expect(wrapper.find('.item').get(0).props.value).toEqual('Widget');
        expect(wrapper.find('.qty').get(0).props.value).toEqual(2);
        expect(wrapper.find('.price').get(0).props.value).toEqual(10);
        expect(wrapper.find('.total').at(0).text()).toContain(20);
    });

    it('should call the callbacks on necessary actions', () => {
        const wrapper = shallow(<DetailsBody tableData={mockTableData} onChange={onChange} onRemove={onRemove}/>);
        wrapper.find('.item').at(0).simulate('change', { target: { value: 'NewItem' } });
        wrapper.find('.onRemove').at(0).simulate('click');

        expect(onChange).toHaveBeenCalledWith({
            key: '1', 
            field: 'item', 
            value: 'NewItem'
        });
        expect(onRemove).toHaveBeenCalled();
    })
});