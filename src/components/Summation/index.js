import React from 'react';
import _ from 'lodash';

const Summation = ({ subtotal, taxPercentage, tax, total, onChangeTaxPercentage }) => (
    <table className="summation">
        <tbody>
            <tr>
                <td>Subtotal</td>
                <td className="subtotal">
                    {_.round(subtotal, 2)}
                </td>
            </tr>
            <tr>
                <td>
                    {`Tax ( `}
                        <input 
                            type="number"
                            className="taxPercentage"
                            onChange={(e) => onChangeTaxPercentage({
                                field: 'taxPercentage', 
                                value: parseInt(e.target.value, 10)
                            })}
                            value={taxPercentage}
                        />
                    {` % )`}
                </td>
                <td className="tax">
                    {_.round(tax, 2)}
                </td>
            </tr>
            <tr>
                <td>Total</td>
                <td className="total">
                    {_.round(total, 2)}
                </td>
            </tr>
        </tbody>
    </table>
);

export default Summation;