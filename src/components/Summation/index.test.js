import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-15';

import Summation from './index';

Enzyme.configure({ adapter: new Adapter() });

const onChangeTaxPercentage = jest.fn(() => {});

describe('Summation', () => {
    it('should contain a table, 3 rows and 2 columns per row', () => {
        const wrapper = shallow(<Summation />);
        expect(wrapper.find('table')).toHaveLength(1);
        expect(wrapper.find('table tr')).toHaveLength(3);
        expect(wrapper.find('table tr td')).toHaveLength(6);
    });

    it('should render the subtotal, tax, total to 2 places to decimal', () => {
        const wrapper = shallow(<Summation subtotal={51.981} tax={2.649} total={54.579}/>);
        expect(wrapper.find('.subtotal').text()).toEqual('51.98');
        expect(wrapper.find('.tax').text()).toEqual('2.65');
        expect(wrapper.find('.total').text()).toEqual('54.58');
    });

    it('should render the taxPercentage to 2 places to decimal', () => {
        const wrapper = shallow(<Summation taxPercentage={5}/>);
        expect(wrapper.find('.taxPercentage').get(0).props.value).toEqual(5);
    });

    it('should call the onChangeTaxPercentage on changing the tax percentage', () => {
        const wrapper = shallow(<Summation onChangeTaxPercentage={onChangeTaxPercentage}/>);
        wrapper.find('.taxPercentage').simulate('change', { target: { value: '10' } });
        expect(onChangeTaxPercentage).toHaveBeenCalledWith({
            field: 'taxPercentage', 
            value: 10
        });
    })
});