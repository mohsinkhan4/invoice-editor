import React from 'react';
import { connect } from 'react-redux';

import '../css/Invoice.css';
import Details from '../components/Details';
import Summation from '../components/Summation';
import { 
    onChange,
    onChangeNew,
    onChangeTaxPercentage,
    onAdd,
    onRemove
} from '../actions';

const Invoice = ({ invoiceItems, onChange, onChangeNew, onChangeTaxPercentage, onAdd, onRemove }) => {

    const { tableData, newData, subtotal, taxPercentage, tax, total } = invoiceItems;
    
    return (
        <div className="container">
            <Details
                tableData={tableData}
                newData={newData}
                onChange={onChange}
                onChangeNew={onChangeNew}
                onAdd={onAdd}
                onRemove={onRemove}
            />
            <div>---------------------------------------------------------------------------</div>
            <Summation 
                subtotal={subtotal}
                taxPercentage={taxPercentage}
                tax={tax}
                total={total}
                onChangeTaxPercentage={onChangeTaxPercentage}
            />
        </div>
    );  
}

const mapStateToProps = (state) => ({
    invoiceItems: state.invoiceItems
});

const mapDispatchToProps = (dispatch) => ({
    onChange: ({ key, field, value }) => dispatch(onChange({ key, field, value })),
    onChangeNew: ({ field, value}) => dispatch(onChangeNew({ field, value })),
    onChangeTaxPercentage: ({ field, value}) => dispatch(onChangeTaxPercentage({ field, value })),
    onAdd: () => dispatch(onAdd()),
    onRemove: (key) => dispatch(onRemove(key))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Invoice);