import { onChange, CHANGE } from './index';

describe('actions', () => {
    it('should generate the correct action on dispatching', () => {
        const { type } = onChange({});
        expect(type).toBe(CHANGE);
    });
});