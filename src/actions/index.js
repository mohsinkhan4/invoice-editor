import uniqid from 'uniqid';

/*
 * action types
 */
export const CHANGE = 'CHANGE';
export const CHANGE_NEW = 'CHANGE_NEW';
export const CHANGE_TAX_PERCENTAGE = 'CHANGE_TAX_PERCENTAGE';
export const ADD = 'ADD';
export const REMOVE = 'REMOVE';

/*
 * action creators
 */

export const onChange = ({ key, field, value }) => {
    return {
        type: CHANGE,
        key,
        field,
        value
    }
}

export const onChangeNew = ({ field, value }) => {
    return {
        type: CHANGE_NEW,
        field,
        value
    }
}

export const onChangeTaxPercentage = ({ field, value }) => {
    return {
        type: CHANGE_TAX_PERCENTAGE,
        field,
        value
    }
}

export const onAdd = () => {
    return {
        type: ADD,
        key: uniqid()
    }
}

export const onRemove = (key) => {
    return {
        type: REMOVE,
        key
    }
}